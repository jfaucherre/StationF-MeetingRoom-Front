(function() {
const demoApp = angular.module('meetingRoomApp', [
    'reservation'
])

/**
 * Main class to request the api
 */
class requestApi {
  static getRoomsByCapacityAndEquipement ($http, capacity, equipements) {
    let request = `${this.apiUrl}/rooms`
    if (capacity)
      request += `/capacity/${capacity}`
    if (equipements && equipements.length)
      request += `/equipements/${JSON.stringify(equipements)}`
    return $http.get(request)
  }

  static getReservationsByRooms ($http, roomId, start, end) {
    return $http.get(`${this.apiUrl}/rooms/${roomId}/reservations/${start.toISOString()}/${end.toISOString()}`)
  }

  static createReservation ($http, roomId, start, end) {
    return $http.post(
      `${this.apiUrl}/reservations`,
      {
        roomId,
        start: start.toISOString(),
        end: end.toISOString()
      })
  }

  static get apiUrl () {
    return 'http://localhost:8080'
  }
}

const reservation = angular.module('reservation', [])

reservation.controller('reservationCtrl', ['$scope', '$http',
  function($scope, $http) {

    let message = $scope.message = 'Selectionner vos criteres'
    let startReservation
    let endReservation
    let rooms = []
    let enabledRooms = $scope.enabledRooms = []

    $scope.possibleEquipements = ['Aucun', 'TV', 'Retro Projecteur']

    /**
     * A little function called when there is a server error
     */
    const serverError = (err) => {
      console.log(err)
      message = $scope.message = 'Il y a eu un probleme, reessayez plus tard'
      enabledRooms = $scope.enabledRooms = []
    }

    /**
     * Main function checking reservation.
     * This function can be called by either findRoom or findReservation.
     * It is going to see if there is laready some reservations for the
     * timing wanted and it will give the available rooms
     */
    const verifyRoom = () => {
      if (!rooms.length || !startReservation || !endReservation) {
        return
      }

      enabledRooms = []
      Promise.all(rooms.map(room => {
        return requestApi.getReservationsByRooms($http, room.id, startReservation, endReservation)
          .then(res => {
            if (res.data.results.length == 0) {
              enabledRooms.push(room)
            }
            return Promise.resolve()
          })
      }))
      .then(() => {
        $scope.enabledRooms = enabledRooms
        if (enabledRooms.length)
          $scope.message = 'Selectionner la salle que vous desirez reserver'
        else (!enabledRooms.length)
          $scope.message = 'Aucune salle n\'est disponible. Essayez un autre horaire'
        $scope.$apply()
      })
      .catch(err => {
        console.log('In verify rooms')
        serverError(err)
      })
    }

    /**
     * Function checking the room with the user criteria:
     *    - capacity
     *    - equipements
     * although it request rooms only when the user has given a capacity
     * It then calls verifyRoom
     */
    $scope.findRoom = (capacity, wantedEquipements) => {

      requestApi.getRoomsByCapacityAndEquipement($http, capacity, wantedEquipements)
      .then(res => {
        rooms = res.data.results

        if (!rooms || !rooms.length) {
          rooms = []
          message = $scope.message = `Il n'y a pas de salles avec une capacite de ${capacity} personnes`
          $scope.enabledRooms = enabledRooms = []
          return
        }

        verifyRoom()

      })
      .catch(err => {
        rooms = []
        serverError(err)
      })
    }

    /**
     * Here is all the constants / predefinitions need by findReservation()
     * The 4 constants represents the reservation characteristics
     * roundTime rounds the time to the nearest quarter
     * dateError is called when there is an error in findReservation()
     * setTime sets the hours/minutes/seconds of dst to the one of src
     */
    const minHours = 8
    const maxHours = 18
    const maxDuration = 2
    const diffDays = 7

    const roundTime = (time) => {
      if (!time)
        return time
      if (time.getMinutes() > 52)
        time.setHours(time.getHours() + 1)
      time.setMinutes((Math.round(time.getMinutes() / 15) * 15) % 60)
      return time
    }

    const dateError = (errorMessage) => {
      message = $scope.message = errorMessage
      startTime = null
      endTime = null
      enabledRooms = $scope.enabledRooms = []
    }

    const setTime = (dst, src) => {
      dst.setHours(src.getHours())
      dst.setMinutes(src.getMinutes())
      dst.setSeconds(src.getSeconds())
    }

    /**
     * Function to check if the given timings are valids
     * Then calls verifyRoom()
     */
    $scope.findReservation = () => {
      let date = $scope.reservationDay
      let startTime = roundTime($scope.startTime)
      let endTime = roundTime($scope.endTime)

      if (!date || !startTime || !endTime)
        return

      const first = new Date()
      const last = new Date()
      first.setHours(first.getHours() + 1)
      last.setDate(last.getDate() + diffDays)
      last.setHours(maxHours)
      last.setMinutes(0)
      last.setSeconds(0)

      const start = new Date(date)
      const end = new Date(date)
      setTime(start, startTime)
      setTime(end, endTime)

      const diff = end - start

      if (start >= end) {
        dateError("L'heure de fin est avant l'heure de debut")
      } else if (diff > maxDuration * 60 * 60 * 1000) {
        dateError("Le creneau choisi est trop long")
      } else if (start < first) {
        dateError("Le debut est trop tot")
      } else if (end > last) {
        dateError("La fin est trop tard")
      } else {

        startReservation = start
        endReservation = end
        verifyRoom()

      }
    }

    $scope.reserveRoom = (room) => {
      requestApi.createReservation($http, room.id, startReservation, endReservation)
      .then(res => {
        $scope.message = `Vous avez reserve la salle ${room.description} le ${startReservation.getDate()}/${startReservation.getMonth() + 1} de ${startReservation.getHours()}:${startReservation.getMinutes()} a ${endReservation.getHours()}:${endReservation.getHours()}`
        $scope.enabledRooms = []
      })
      .catch(serverError)
    }

  }
])
})()
